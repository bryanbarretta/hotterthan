'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'home/home.html',
    controller: 'HomeCtrl'
  });
}])

.controller('HomeCtrl', ['$scope', '$http', function($scope, $http) {

  $scope.units = "metric";

  $scope.apiWeather = "21d4b07bfb4b9ad55bf816db2632b115"; //https://openweathermap.org/api

  $scope.template = "Ireland {0} as the Mercury rocketed to {1} degrees Celsius, hotter than {2}";

  $scope.synonyms = ["sizzling", "scorching", "sweltering", "stewing", "roasting", "scalding", "boiling hot", "very hot",
  "toasty", "searing", "piping hot", "blazing"];

  //full list of city id's: http://bulk.openweathermap.org/sample/city.list.json.gz
  //London,Paris,Istanbul,Rome,Prague,Amsterdam,Vienna,Milan,Berlin,Moscow,Madrid,
  //Florence,Saint Petersburg,Brussels,Munich,Budapest,Copenhagen,Warsaw,Stockholm,Nice,Helsinki
  $scope.cityIds = [2643743, 2968815, 745044, 3169070, 2759794, 2761369, 3173435, 2950158, 524894, 3117735,
    3176959, 498817, 2800866, 2867714, 3054638, 2618425, 756135, 2673722, 2990440, 658225];

  let url_base = "https://api.openweathermap.org/data/2.5/";
  let url_city = url_base + "weather";
  let url_cities = url_base + "group";
  let api_key_app_id = "appid";
  let api_key_city_id = "id";
  let api_key_city_country = "q";
  let api_key_units = "units";

  $scope.output = "Click button to generate a headline";

  $scope.fetchData = function(){
    let urlDublin = url_city + "?" +
        api_key_city_country + "=" + "Dublin,ie" + "&" +
        api_key_units + "=" + $scope.units + "&" +
        api_key_app_id + "=" + $scope.apiWeather;
    let urlEurope = url_cities + "?" +
        api_key_city_id + "=" + $scope.cityIds.join(",") + "&" +
        api_key_units + "=" + $scope.units + "&" +
        api_key_app_id + "=" + $scope.apiWeather;

    $http({ method: 'GET', url: urlDublin}).then(function (resDublin){
      $scope.dataIreland = resDublin;
      $http({ method: 'GET', url: urlEurope}).then(function (resEurope){
        $scope.dataEurope = resEurope;
        renderSentence();
      },function (errorEurope){
        alert("errorEurope: " + errorEurope);
      });
    },function (errorDublin){
      alert("errorDublin: " + errorDublin);
    });
  };

  let renderSentence = function () {
    let temperatureDublin = $scope.dataIreland["data"]["main"]["temp"];
    let temperatureEurope = $scope.dataEurope["data"]["list"];
    let filteredColderThanDublin = temperatureEurope.filter(x => x["main"]["temp"] < temperatureDublin)
    let combined = "";
    for(let i = 0; i < filteredColderThanDublin.length; i++){
      let p = filteredColderThanDublin[i];
      if(combined.length > 1){
        combined += ", ";
      }
      combined += p.name + " (" + p.main.temp + ")";
    }
    const random = Math.floor(Math.random() * $scope.synonyms.length);
    let desc = $scope.synonyms[random];

    $scope.output = "Ireland " + desc.toString().toUpperCase() + " as the Mercury rockets to " + temperatureDublin + " degrees Celsius, hotter than " + combined;
  }

}]);